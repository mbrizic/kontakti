﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Kontakti.Models
{
    public class ContactsDBContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public DbSet<MailAddress> MailAddresses { get; set; }
        public DbSet<Tag> Tags { get; set; }
    }
}