﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kontakti.Models
{
    public class Tag
    {

        public Tag() { 
            
        }

        public Tag(string tagName) {
            TagName = tagName;
        }
        public int Id { get; set; }
        public int ContactId { get; set; }

        [Required]
        [Display(Name = "Tagovi")]
        public string TagName { get; set; }
    }
}