﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kontakti.Models
{
    public class PhoneNumber
    {
        public int Id { get; set; }
        public int ContactId { get; set; }

        [Required]
        [Display(Name = "Broj telefona")]
        public string Number { get; set; }

    }
}