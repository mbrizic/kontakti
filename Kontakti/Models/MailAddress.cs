﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kontakti.Models
{
    public class MailAddress
    {
        public int Id { get; set; }
        public int ContactId { get; set; }

        [Required]
        [Display(Name = "E-mail adresa")]
        public string Address { get; set; }
    }
}