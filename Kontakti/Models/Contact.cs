﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kontakti.Models
{
    public class Contact
    {
        public Contact()
        {
            PhoneNumbers = new List<PhoneNumber>();
            MailAddresses = new List<MailAddress>();
            Tags = new List<Tag>();
        }

        public int Id { get; set; }

        [Display(Name="Ime")]
        public string FirstName { get; set; }

        [Display(Name = "Prezime")]
        public string LastName { get; set; }

        [Display(Name = "Adresa")]
        public string Address { get; set; }

        [Display(Name = "Favorit")]
        public bool Favourite { get; set; }

        public virtual ICollection<PhoneNumber> PhoneNumbers { get; set; }
        public virtual ICollection<MailAddress> MailAddresses { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }


    }



}