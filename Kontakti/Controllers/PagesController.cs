﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kontakti.Controllers
{
    public class PagesController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error() {
            return View();
        }

    }
}
