﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Kontakti.Models;

namespace Kontakti.Controllers
{
    public class ContactsApiController : ApiController
    {
        private ContactsDBContext db = new ContactsDBContext();

        // GET api/Api
        public IEnumerable<Contact> GetContacts()
        {
            return db.Contacts.AsEnumerable();
        }

        // GET api/ContactsApi/Search
        [HttpGet]
        public IEnumerable<Contact> Search(string term = "")
        {
            if (term == "")
                return db.Contacts.AsEnumerable();
            else
                return db.Contacts.Where(r => r.FirstName.Contains(term) || r.LastName.Contains(term) || 
                                              r.Tags.Any(t => t.TagName == term)).AsEnumerable();
        }

        // GET api/Api/5
        public Contact GetContact(int id)
        {
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return contact;
        }

        // PUT api/Api/5
        [HttpPut, HttpPatch]
        public HttpResponseMessage PutContact(int id, Contact contact)
        {
            
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != contact.Id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(contact).State = EntityState.Modified;

            //ručno dodan kod, EF automatski radi update samo skalarnih svojstava elementata. Za liste je potrebno postaviti ručno.

            foreach (var phoneNumber in contact.PhoneNumbers)
                db.Entry(phoneNumber).State = EntityState.Modified;
            
            foreach (var mailAddress in contact.MailAddresses)
                db.Entry(mailAddress).State = EntityState.Modified;
            

            foreach (var tag in contact.Tags)
                db.Entry(tag).State = EntityState.Modified;





            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Api
        [HttpPost]
        public HttpResponseMessage PostContact(Contact contact)
        {
            //trebamo Id korisnika za održavanje relacija u bazi. Angular ga ne može znati, pa to postavljamo tu:
            foreach (var phoneNumber in contact.PhoneNumbers) {  phoneNumber.ContactId = contact.Id;}
            foreach (var mail in contact.MailAddresses) { mail.ContactId = contact.Id; }

            if (ModelState.IsValid)
            {
                db.Contacts.Add(contact);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, contact);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = contact.Id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Api/5
        [HttpPost]
        public HttpResponseMessage DeleteContact(int id)
        {
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Contacts.Remove(contact);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, contact);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}