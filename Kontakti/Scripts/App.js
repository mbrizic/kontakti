﻿var App = angular.module('App', ['ngRoute']);

App.controller('ContactsController', ContactsController);


var configFunction = function ($routeProvider) {
    $routeProvider.
        when('/', {
            templateUrl: 'partials/index.html',
            controller: 'ContactsController'
        })
        .when('/kontakti', {
            templateUrl: 'partials/index.html',
            controller: 'ContactsController'
        })
        .when('/dodaj', {
            templateUrl: 'partials/create.html',
            controller: 'ContactsController'
        })
        .when('/trazi', {
            templateUrl: 'partials/search.html',
            controller: 'ContactsController'
        })
        .when('/kontakt/:id', {
            templateUrl: 'partials/details.html',
            controller: 'ContactsController'
        })
        .when('/uredi/:id', {
            templateUrl: 'partials/edit.html',
            controller: 'ContactsController'
        })
        .otherwise({
            templateUrl: '/Pages/Error'
        });;
}
configFunction.$inject = ['$routeProvider'];

App.config(configFunction);