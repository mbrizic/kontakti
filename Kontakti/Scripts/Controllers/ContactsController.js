﻿var ContactsController = function ($scope, $route, $http, $location) {

    $scope.$on('$routeChangeSuccess', function () {
        var template = $route.current.templateUrl;
        var params = $route.current.params.id;

        //postavi korisnika za prikaz
        if (template == 'partials/details.html' || template == 'partials/edit.html')
            $scope.contact = $scope.getContact(params);
     
    });

    $scope.getAllContacts = function () {
        $http.get('/api/ContactsApi/GetContacts').then(function (resp) {
            $scope.contacts = resp.data
        });
    }

    $scope.getContact = function (id) {
        $http.get('/api/ContactsApi/GetContact/'+ id).then(function (resp) {
            $scope.contact = resp.data
            console.log($scope.contact);
        });
    }

    $scope.deleteContact = function (id) {
        $http.post('/api/ContactsApi/DeleteContact/' + id).
         success(function (data, status, headers, config) {
             console.log(data);
             removeItemWithIndexFrom($scope.contacts);
         }).
         error(function (data, status, headers, config) {
             console.log(data);
         });
    }

    $scope.search = function (term) {

        $scope.searchTerm = term;
        $scope.searched = true;

        $http.get('/api/ContactsApi/Search/' + term).then(function (resp) {
            $scope.searchResults = resp.data            
            console.log(resp.data);
        });
    }


    $scope.submit = function (form) {

        if (form.$invalid) {
            return;
        }

        $scope.newContact = {}
        $scope.newContact = {
            'FirstName': $scope.FirstName,
            'LastName': $scope.LastName,
            'Favourite': $scope.Favourite,
            'Address': $scope.Address
        };

        addToNewContactIfFieldExists($scope.MailAddress, 'MailAddresses', 'Address')
        addToNewContactIfFieldExists($scope.Tags, 'Tags', 'TagName')
        addToNewContactIfFieldExists($scope.Number, 'PhoneNumbers', 'Number')
        

        $http.post('/api/ContactsApi/PostContact', $scope.newContact ).
         success(function (data, status, headers, config) {
             console.log(data);
             $scope.getAllContacts();
             $location.path('/');
         }).
         error(function (data, status, headers, config) {
             console.log(data);
         });

        $scope.newContact = {};

    }


    $scope.update = function (form) {
        
        if (form.$invalid)
            return;

        $http.put('api/ContactsApi/PutContact/' + $scope.contact.Id, $scope.contact).
             success(function (data, status, headers, config) {
                 console.log(data);
                 $location.path('/');
             }).
             error(function (data, status, headers, config) {
                 console.log(data);
             });
    }


    var init = function () {
        $scope.getAllContacts();
        $scope.searched = false;
    }

    var removeItemWithIndexFrom = function (arr, id) {
        return _.without(arr, _.findWhere(arr, { id: id }));
    }

    var splitStringToArray = function(input){        
        return _.map(input.split(','), function (a) { return a.trim() })
    }

    var arrayToString = function (input, property) {
        var output = "";

        output = _.each(input, function (a) { output += a[property] + ", "; console.log(a) })
        return output
    }

    var addToNewContactIfFieldExists = function(modelVariable, listName, fieldName){
        if (modelVariable) {
            var items = splitStringToArray(modelVariable)
            $scope.newContact[listName] = []

            _.each(items, function (a) {
                var newObj = {}
                newObj[fieldName] = a

                $scope.newContact[listName].push(newObj);
            })
        }
    
    }

    init();

}

ContactsController.$inject = ['$scope', '$route', '$http', '$location'];