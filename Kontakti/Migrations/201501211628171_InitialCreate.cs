namespace Kontakti.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Favourite = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhoneNumbers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactId = c.Int(nullable: false),
                        Broj = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.MailAddresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactId = c.Int(nullable: false),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactId = c.Int(nullable: false),
                        TagName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tags", new[] { "ContactId" });
            DropIndex("dbo.MailAddresses", new[] { "ContactId" });
            DropIndex("dbo.PhoneNumbers", new[] { "ContactId" });
            DropForeignKey("dbo.Tags", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.MailAddresses", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.PhoneNumbers", "ContactId", "dbo.Contacts");
            DropTable("dbo.Tags");
            DropTable("dbo.MailAddresses");
            DropTable("dbo.PhoneNumbers");
            DropTable("dbo.Contacts");
        }
    }
}
