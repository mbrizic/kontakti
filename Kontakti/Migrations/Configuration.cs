namespace Kontakti.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Kontakti.Models;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<Kontakti.Models.ContactsDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Kontakti.Models.ContactsDBContext context)
        {

            context.Contacts.AddOrUpdate(
                new Contact
                {
                    FirstName = "Prvi",
                    LastName = "Drugi",
                    Favourite = true,
                    Address = "Nasumicna Ulica 123",
                    PhoneNumbers = new List<PhoneNumber> { 
                        new PhoneNumber { Number = "09165189"},
                        new PhoneNumber { Number = "021215156"},
                    }
                },
                new Contact { 
                    FirstName = "John", 
                    Favourite = true, 
                    Address = "Random Lane 456", 
                    PhoneNumbers = new List<PhoneNumber> { 
                        new PhoneNumber { Number = "241235"},
                    } 
                },
                new Contact
                {
                    LastName = "Peric",
                    Favourite = false,
                    Address = "Na kraju",
                    PhoneNumbers = new List<PhoneNumber> { 
                        new PhoneNumber { Number = "323241235"},
                    }
                }
            );
                        
            
        }
    }
}
